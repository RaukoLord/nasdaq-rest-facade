# Summary

RESTful service that provides access to data from NASDAQ.
This service supports caching of data.

# Tools

* Version Control - [Git 2.6.x](https://git-scm.com/)
* Build Tool - [Maven 3.3.x](https://maven.apache.org/)
* Web Server - [Tomcat 8.0.x](http://tomcat.apache.org/)
* In-Memory Database - [Hazelcast 3.5.x](http://hazelcast.org/)

# Libraries

* RESTful Web Service - [Jersey 2.x](https://jersey.java.net/)
* Dependency Injection - [Guice 4.x](https://github.com/google/guice)
* HTML Parser - [jsoup 1.8.x](http://jsoup.org/)

# Build and deploy

Add following lines to `conf/tomcat-users.xml`, to create user on Tomcat 7/8:

```xml
<role rolename="manager-script"/>
<user username="nasdaq" password="aaAA11!!" roles="manager-script"/>
```

Use following command to build and deploy application:

```
mvn tomcat7:deploy -Dtomcat.username=nasdaq -Dtomcat.password=aaAA11!!
```
