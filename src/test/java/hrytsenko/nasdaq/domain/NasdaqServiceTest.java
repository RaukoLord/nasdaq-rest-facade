package hrytsenko.nasdaq.domain;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import hrytsenko.nasdaq.domain.data.NasdaqStock;
import hrytsenko.nasdaq.endpoint.NasdaqEndpoint;
import hrytsenko.nasdaq.error.ApplicationException;

public class NasdaqServiceTest {

    private static final NasdaqStock MSFT = new NasdaqStock("msft", "Technology", "$50.00");
    private static final NasdaqStock ORCL = new NasdaqStock("orcl", "Technology", "$35.00");

    @Mock
    private CacheService cacheService;
    @Mock
    private NasdaqEndpoint nasdaqEndpoint;

    @InjectMocks
    private NasdaqService nasdaqService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        Mockito.doReturn(Optional.empty()).when(cacheService).findStock(Matchers.anyString());
        Mockito.doReturn(Optional.of(MSFT)).when(cacheService).findStock(MSFT.getSymbol());

        Mockito.doThrow(ApplicationException.class).when(nasdaqEndpoint).loadStock(Matchers.anyString());
        Mockito.doReturn(ORCL).when(nasdaqEndpoint).loadStock(ORCL.getSymbol());
    }

    @Test
    public void testLoadFromCache() {
        NasdaqStock msft = nasdaqService.findStock(MSFT.getSymbol());

        Assert.assertSame(MSFT, msft);

        Mockito.verify(cacheService).findStock(msft.getSymbol());
        Mockito.verifyNoMoreInteractions(cacheService, nasdaqEndpoint);
    }

    @Test
    public void testLoadFromEndpoint() {
        NasdaqStock orcl = nasdaqService.findStock(ORCL.getSymbol());

        Assert.assertSame(ORCL, orcl);

        Mockito.verify(cacheService).findStock(ORCL.getSymbol());
        Mockito.verify(cacheService).saveStock(ORCL);
        Mockito.verify(nasdaqEndpoint).loadStock(ORCL.getSymbol());
        Mockito.verifyNoMoreInteractions(cacheService, nasdaqEndpoint);
    }

}
