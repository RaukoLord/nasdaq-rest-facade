package hrytsenko.nasdaq.system;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import com.google.common.collect.Maps;
import com.google.common.io.Resources;

import hrytsenko.nasdaq.error.ApplicationException;

public class SettingsService {

    private static final Logger LOGGER = Logger.getLogger(SettingsService.class.getName());

    private static final String SETTINGS_FILE = "app.properties";

    private Map<String, String> settings;

    private static Map<String, String> loadSettings(String filename) {
        try {
            Properties properties = new Properties();
            properties.load(new ByteArrayInputStream(Resources.toByteArray(Resources.getResource(filename))));
            return Maps.fromProperties(properties);
        } catch (IOException exception) {
            throw new ApplicationException("Could not read properties.", exception);
        }
    }

    public void startup() {
        LOGGER.info("Initialize settings.");

        this.settings = Collections.unmodifiableMap(loadSettings(SETTINGS_FILE));
    }

    public String getStockLink() {
        return get("stock_link");
    }

    public int getStockTimeToLive() {
        return Integer.parseInt(get("stock_ttl"));
    }

    private String get(String name) {
        return settings.get(name);
    }

}
