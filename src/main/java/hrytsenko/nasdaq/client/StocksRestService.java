package hrytsenko.nasdaq.client;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import hrytsenko.nasdaq.domain.NasdaqService;
import hrytsenko.nasdaq.domain.data.NasdaqStock;

@Path("stocks")
@Produces(MediaType.APPLICATION_JSON)
public class StocksRestService {

    @Inject
    private NasdaqService nasdaqService;

    @GET
    @Path("{symbol}")
    public NasdaqStock quote(@PathParam("symbol") String symbol) {
        return nasdaqService.findStock(symbol);
    }

}
