package hrytsenko.nasdaq.domain;

import java.util.logging.Logger;

import javax.inject.Inject;

import hrytsenko.nasdaq.domain.data.NasdaqStock;
import hrytsenko.nasdaq.endpoint.NasdaqEndpoint;

public class NasdaqService {

    private static final Logger LOGGER = Logger.getLogger(NasdaqService.class.getName());

    @Inject
    private CacheService cacheService;
    @Inject
    private NasdaqEndpoint nasdaqEndpoint;

    public NasdaqStock findStock(String symbol) {
        LOGGER.info(() -> String.format("Find data for %s.", symbol));

        return cacheService.findStock(symbol).orElseGet(() -> loadStock(symbol));
    }

    private NasdaqStock loadStock(String symbol) {
        NasdaqStock stock = nasdaqEndpoint.loadStock(symbol);
        cacheService.saveStock(stock);
        return stock;
    }

}
