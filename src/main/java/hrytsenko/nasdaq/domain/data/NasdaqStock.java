package hrytsenko.nasdaq.domain.data;

import java.io.Serializable;

public class NasdaqStock implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String symbol;
    private final String sector;
    private final String lastSale;

    public NasdaqStock(String symbol, String sector, String lastSale) {
        this.symbol = symbol;
        this.sector = sector;
        this.lastSale = lastSale;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getSector() {
        return sector;
    }

    public String getLastSale() {
        return lastSale;
    }

}
