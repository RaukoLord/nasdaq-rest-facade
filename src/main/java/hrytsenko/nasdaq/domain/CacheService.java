package hrytsenko.nasdaq.domain;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import javax.inject.Inject;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import hrytsenko.nasdaq.domain.data.NasdaqStock;
import hrytsenko.nasdaq.system.SettingsService;

public class CacheService {

    private static final Logger LOGGER = Logger.getLogger(CacheService.class.getName());

    private static final String STOCKS = "stocks";

    @Inject
    private SettingsService settingsService;

    private Map<String, NasdaqStock> stocks;

    public void startup() {
        LOGGER.info("Initialize cache.");

        MapConfig stocksConfig = new MapConfig();
        stocksConfig.setTimeToLiveSeconds(settingsService.getStockTimeToLive());

        Config config = new Config();
        config.setMapConfigs(Collections.singletonMap(STOCKS, stocksConfig));

        HazelcastInstance cache = Hazelcast.newHazelcastInstance(config);
        stocks = cache.getMap(STOCKS);
    }

    public void shutdown() {
        Hazelcast.shutdownAll();
    }

    public Optional<NasdaqStock> findStock(String symbol) {
        return Optional.ofNullable(stocks.get(symbol));
    }

    public void saveStock(NasdaqStock stock) {
        stocks.putIfAbsent(stock.getSymbol(), stock);
    }

}
