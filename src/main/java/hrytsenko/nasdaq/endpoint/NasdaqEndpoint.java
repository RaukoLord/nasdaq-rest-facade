package hrytsenko.nasdaq.endpoint;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import hrytsenko.nasdaq.domain.data.NasdaqStock;
import hrytsenko.nasdaq.error.ApplicationException;
import hrytsenko.nasdaq.system.SettingsService;

public class NasdaqEndpoint {

    private static final Logger LOGGER = Logger.getLogger(NasdaqEndpoint.class.getName());

    @Inject
    private SettingsService settingsService;

    public NasdaqStock loadStock(String symbol) {
        LOGGER.info(() -> String.format("Load data for %s.", symbol));

        try {
            String link = settingsService.getStockLink().replace("{symbol}", symbol);
            Document document = Jsoup.connect(link).get();

            checkThatSymbolWasFound(document);

            String sector = textOf(document, "#qbar_sectorLabel > a").orElse("");
            String lastSale = textOf(document, "#qwidget_lastsale").orElse("");

            return new NasdaqStock(symbol, sector, lastSale);
        } catch (IOException exception) {
            throw new ApplicationException(String.format("Could not load data for %s.", symbol), exception);
        }
    }

    private static void checkThatSymbolWasFound(Document document) {
        if (tryFind(document, ".notTradingIPO").isPresent()) {
            throw new ApplicationException("Stock not found.");
        }
    }

    private static Optional<String> textOf(Document document, String cssQuery) {
        return tryFind(document, cssQuery).map(element -> element.text());
    }

    private static Optional<Element> tryFind(Document document, String cssQuery) {
        return document.select(cssQuery).stream().findFirst();
    }

}
