package hrytsenko.nasdaq.web;

import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContextEvent;

import com.google.inject.Module;
import com.google.inject.servlet.ServletModule;
import com.squarespace.jersey2.guice.JerseyGuiceServletContextListener;

import hrytsenko.nasdaq.client.StocksRestService;
import hrytsenko.nasdaq.domain.CacheService;
import hrytsenko.nasdaq.system.SettingsService;

public class ApplicationListener extends JerseyGuiceServletContextListener {

    @Override
    protected List<Module> modules() {
        return Arrays.asList(new ServletModule() {
            protected void configureServlets() {
                bind(SettingsService.class).asEagerSingleton();
                bind(CacheService.class).asEagerSingleton();
                bind(StocksRestService.class);
            }
        });
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        getInjector().getInstance(SettingsService.class).startup();
        getInjector().getInstance(CacheService.class).startup();
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        getInjector().getInstance(CacheService.class).shutdown();
    }

}
